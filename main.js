var socket = io("http://cryptic-retreat-54550.herokuapp.com/");
var player={clicks:0,anim:{btn:0},upgrades:[{type:"none"},{type:"none"},{type:"none"}]}
let opponent={id:0}
let cursorIMG
var screen=0
var anim=[0,0]
var started=false
var winner=null
var deck=["Auto Clicker","Click Power","Attack Power"]
var pages=[{title:"Click Combat: The First One",cards:["Auto Clicker","Click Power","Attack Power","Discount","Smite","Haggle","Frost","Interest"]}]
window.onbeforeunload = function(){
   socket.emit("quit",{});
}
function setup(){
	cursorIMG=loadImage("cursor.png")
	createCanvas(windowWidth-10,windowHeight-20)
	socket.on('heartbeat',function(data){
		for(var i=0;i<data.games.length;i++){
			if(data.games[i].winner=="none"){ 
				for(var j=0;j<data.games[i].players.length;j++)if(data.games[i].players[j].id==socket.id){
					player=data.games[i].players[j]
					opponent=data.games[i].players[1-j]
				}
			}else if(data.games[i].winner==socket.id){
				winner=player
			}else if(data.games[i].winner==opponent.id){
				winner=opponent
			}
		}
	})
}
desc={}
desc["Auto Clicker"]="Gain 2 clicks every second"
desc["Click Power"]="Clicking your button gives you an extra click"
desc["Attack Power"]="Clicking on the opponent's button deals 2 damage"
desc["Discount"]="All upgrade prices go up 10% slower"
desc["Smite"]="Instantly click on the opponents button 15 times"
desc["Haggle"]="Click an upgrade to make it cost 3 less"
desc["Frost"]="+20% chance to keep an upgrade after purchase"
desc["Interest"]="Gain 1% of your total clicks every second (Max. 10 per second)"
function drawupgrade(x,y,type,price,anim){
	fill(255,255,0)
	if(type=="none")return
	push();
	translate(x-width/40,y-width/30)
	scale(1-anim)
	fill(200)
	rect(-width/20,-width/20,width/10,width/10)
	push();
	if(type=="Auto Clicker"){
		translate(0,Math.abs(sin(millis()/300)**3)*width/40-16)
		rotate(2.7)
	}
	
	if(!(["Discount","Interest","Smite","Frost"].includes(type)))image(cursorIMG,width/40,width/30,-width/20,-width/15)
	pop();
	if(type=="Frost"){
		push();
		noStroke();
		fill(155*sin(millis()/500),155,255)
		rotate(millis()/2000)
		fill(155*cos(millis()/500),155,255,255)
		for(var i=1;i<10;i+=2)ellipse(0,0,Math.max(10,width/1000*i*i),Math.max(10,width/1000*(10-i)*(10-i)))
		rotate(PI*7/4)
		
		for(var i=1;i<10;i+=2)ellipse(0,0,Math.max(10,width/1000*i*i),Math.max(10,width/1000*(10-i)*(10-i)))
		rotate(PI/4)
		fill(155*cos(millis()/500),155,255,128)
		for(var i=1;i<10;i+=2)ellipse(0,0,Math.max(10,width/1000*i*i),Math.max(10,width/1000*(10-i)*(10-i)))
		pop();
	}
	if(type=="Attack Power"){
		push();
		fill(255,100*sin(millis()/250),0,50+100*sin(millis()/500))
		translate(-width/40,-width/30)
		rotate(millis()/500)
		noStroke();
		for(var i=0;i<10;i+=2)ellipse(0,0,width/200*i,width/200*(10-i))
		pop();
	}
	if(type=="Click Power"){
		push()
		fill(0,255,0)
		translate(width/40,width/40)
		rotate(sin(millis()/300)/3)
		stroke(0,255,0)
		rect(-width/60,-2,width/30,4)
		rect(-3,-width/60,6,width/30)
		translate(-2,-2)
		stroke(0,200,0)
		fill(0,200,0)
		rect(-width/60,-2,width/30,4)
		rect(-3,-width/60,6,width/30)
		pop()
	}
	if(type=="Discount"){
		fill(sin(millis()/500)**3*100,200,sin(millis()/500)**3*100)
		textSize(width/20)
		text("$$$",-width/25,width/60)
		fill(sin(1+millis()/500)**3*100,255,sin(1+millis()/500)**3*100)
		text("$$$",-width/27,width/70)
	}
	if(type=="Interest"){
		
		noStroke()
		fill(0,255,0)
		triangle(width/80,-width/25,width*3/80,-width/25,width/40,-width/15)
		for(var i=0;i<PI/6;i+=.01){
			
			j=(Math.floor(millis()/10)/100+i)%1
			if(j<.5)ellipse((j-.5)*width/20,map(j,0,.5,width/20,0),width/100,width/100)
			else if(j<.75)ellipse((j-.5)*width/20,map(j,.5,.75,0,width/40),width/100,width/100)
			else ellipse((j-.5)*width/20,map(j,.75,1,width/40,-width/20),width/100,width/100)
		}
	}
	if(type=="Smite"){
		
		for(var i=0;i<25;i++){
			stroke(230+random(25),230+random(25),0)
			strokeWeight(5)
			x1=random(-width/200,width/200)+width/40
			y1=random(-width/200,width/200)+width/60
			x2=random(-width/200,width/200)-width/40
			y2=random(-width/200,width/200)-width/60
			line(-width/30,width/30,x1,y1)
			line(x1,y1,x2,y2)
			line(x2,y2,width/30,-width/30)
		}
	}
	if(type=="Haggle"){
		push();
		rotate(-.4)
		textSize(width/130*(sin(millis()/500)+1))
		fill(255,0,0)
		text("$$$",-width/100-textWidth("$$$")/2,0)
		pop();
	}
	noStroke()
	textSize(width/40)
	fill(175,175,0)
	text(price,2-textWidth(price)/2,width/12)
	textSize(width/60)
	
	text(type,-textWidth(type)/2,-width/16)
	textSize(width/40)
	fill(255,255,0)
	text(price,width/1000-textWidth(price)/2,width/1000+width/12)
	textSize(width/60)
	
	text(type,width/1000-textWidth(type)/2,width/1000-width/16)
	pop();
}
function mousePressed(){
	if(screen==2){
		if(mouseX>width*.14&&mouseX<width*.36&&mouseY>height*.4-width*.06&&mouseY<height*.4+width*.06)socket.emit("action",{type:"click"});
		if(mouseX>width*.64&&mouseX<width*.86&&mouseY>height*.4-width*.06&&mouseY<height*.4+width*.06)socket.emit("action",{type:"attack"});
		if(mouseX>width*.1&&mouseX<width*.4&&mouseY>height/32&&mouseY<height/32+height/5)socket.emit("action",{type:"reroll"});
		if(mouseX>width*.1-width/20&&mouseX<width*.1+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)socket.emit("action",{type:"upgrade",index:0});
		if(mouseX>width*.275-width/20&&mouseX<width*.275+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)socket.emit("action",{type:"upgrade",index:1});
		if(mouseX>width*.45-width/20&&mouseX<width*.45+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)socket.emit("action",{type:"upgrade",index:2});
		if(mouseX>width/3,mouseX<width*2/3,mouseY>height*.7,mouseY<height*.9&&(opponent.id==0)){
			screen=0;
			socket.emit("quit",{});
		}
	}else if(screen==1){
		var i=0
		Object.keys(desc).forEach(function(key) {
			i++
			textSize(width/40)
			if(mouseX>width/30+i*width/10-width/20&&mouseX<width/30+i*width/10+width/20&&mouseY>(i%2)*width/6+width/8-width/20&&mouseY<(i%2)*width/6+width/8+width/20)deck.push(key)
		});
		if(mouseY>height*3/4)deck.splice(Math.floor(mouseX*10/width),1)
		if(mouseY<height*.06&&mouseX<width*.1){
			if(deck.length>0)screen=0
			anim[0]=0
			if(deck.length==0)anim[0]=255
		}
	}else if(screen==0){
		if(mouseX>width/12&&mouseX<width*5/12&&mouseY>height*.4&&mouseY<height*.9){
			screen=1
			anim[0]=0
			anim[1]=0
		}if(mouseX>width*7/12&&mouseX<width*11/12&&mouseY>height*.4&&mouseY<height*.9){
			screen=2
			var player={clicks:0,anim:{btn:0},upgrades:[{type:"none"},{type:"none"},{type:"none"}]}
			opponent={id:0}
			winner=null
			socket.emit("join",{deck})
		}
	}
}
function mouseMoved(){
	socket.emit("move",{x:mouseX/width,y:mouseY/height})
}
function title(){
	background(0)
	player={clicks:0,anim:{btn:0},upgrades:[{type:"none"},{type:"none"},{type:"none"}]}
	if(mouseX>width/12&&mouseX<width/3&&mouseY>height*.4&&mouseY<height*.9)anim[0]+=3
	if(mouseX>width*7/12&&mouseX<width*11/12&&mouseY>height*.4&&mouseY<height*.9)anim[1]+=3
	anim[0]*=.95
	anim[1]*=.95
	textSize(width/12)
	fill(255,155,0)
	text("Click Combat Beta",width/2-textWidth("Click Combat Beta")/2,width/8)
	fill(255,255,0)
	text("Click Combat Beta",width/2-textWidth("Click Combat Beta")/2-width/400,width/8-width/400)
	fill(200+anim[0])
	rect(width/12-anim[0],height*.4-anim[0],width/3+anim[0]*2,height/2+anim[0]*2)
    fill(200+anim[1])
	rect(width*7/12-anim[1],height*.4-anim[1],width/3+anim[1]*2,height/2+anim[1]*2)
	textSize(width/16+anim[0])
	fill(0)
	text(" Deck\nBuilder",width/4-textWidth("Builder")/2,height*.6)
	fill(100)
	text(" Deck\nBuilder",width/4-textWidth("Builder")/2-width/400,height*.6-width/400)
	fill(0)
	textSize(width/16+anim[1])
	text(" Find\nMatch",width*.75-textWidth("Match")/2,height*.6)
	fill(100)
	text(" Find\nMatch",width*.75-textWidth("Match")/2-width/400,height*.6-width/400)
}
function game(){
	
	background(100)
	
	strokeWeight(8)
	line(width/2,0,width/2,height)
	drawupgrade(width*.1,height*.85,player.upgrades[0].type,int(player.upgrades[0].price)+" Clicks",player.anim.upgrade0)
	drawupgrade(width*.275,height*.85,player.upgrades[1].type,int(player.upgrades[1].price)+" Clicks",player.anim.upgrade1)
	drawupgrade(width*.45,height*.85,player.upgrades[2].type,int(player.upgrades[2].price)+" Clicks",player.anim.upgrade2)
	
	if(opponent.id!=0){
		fill(0)
		push()
		fill(200)
		rect(width*.1,height/32,width*.3,height/5)
		fill(200,200,0)
		textSize(100)
		text("Reroll",width*.25-textWidth("Reroll")/2,height/7)
		textSize(30)
		text(5**player.rerolls+" Clicks",width*.25-textWidth(5**player.rerolls+" Clicks")/2,height*.2)
		fill(255,255,0)
		textSize(100)
		text("Reroll",width*.2505-textWidth("Reroll")/2,width/200+height/7)
		textSize(30)
		text(5**player.rerolls+" Clicks",width*.2505-textWidth(5**player.rerolls+" Clicks")/2,width/200+height*.2)
		translate(width*.5,0)
		fill(200)
		rect(width*.1,height/32,width*.3,height/5)
		fill(200,200,0)
		textSize(100)
		text("Reroll",width*.25-textWidth("Reroll")/2,height/7)
		textSize(30)
		text(5**opponent.rerolls+" Clicks",width*.25-textWidth(5**opponent.rerolls+" Clicks")/2,height*.2)
		fill(255,255,0)
		textSize(100)
		text("Reroll",width*.2505-textWidth("Reroll")/2,width/200+height/7)
		textSize(30)
		text(5**opponent.rerolls+" Clicks",width*.2505-textWidth(5**opponent.rerolls+" Clicks")/2,width/200+height*.2)
		pop();
		drawupgrade(width*.6,height*.85,opponent.upgrades[2].type,int(opponent.upgrades[2].price)+" Clicks",opponent.anim.upgrade2)
		drawupgrade(width*.775,height*.85,opponent.upgrades[1].type,int(opponent.upgrades[1].price)+" Clicks",opponent.anim.upgrade1)
		drawupgrade(width*.95,height*.85,opponent.upgrades[0].type,int(opponent.upgrades[0].price)+" Clicks",opponent.anim.upgrade0)
	}
	push();
	translate(width*.25,height*.4);
	scale(player.anim.btn+1)
	
	strokeWeight(2)
	fill(0,155,0)
	rect(-width*.11,-width*.06,width*.22,width*.12)
	fill(0,255,0)
	strokeWeight(4)
	rect(-width*.10,-width*.05,width*.2,width*.1)
	
	
	fill(0)
	textSize((player.anim.btn+1)*width/10)
	text(""+int(player.clicks),-textWidth(""+int(player.clicks))/2,width/30)
	fill(20)
	text(""+int(player.clicks),width*.003-textWidth(""+int(player.clicks))/2,width/30)
	pop();
	if(opponent.id!=0||winner){
		
		push();
		translate(width*.75,height*.4);
		scale(opponent.anim.btn+1)
		
		strokeWeight(2)
		fill(155,0,0)
		rect(-width*.11,-width*.06,width*.22,width*.12)
		fill(255,0,0)
		strokeWeight(4)
		rect(-width*.10,-width*.05,width*.2,width*.1)
		
		fill(0)
		textSize((opponent.anim.btn+1)*width/10)
		text(""+int(opponent.clicks),-textWidth(""+int(opponent.clicks))/2,width/30)
		fill(20)
		text(""+int(opponent.clicks),width*.003-textWidth(""+int(opponent.clicks))/2,width/30)
		pop();
	}else{
		fill(200)
		textSize(width/15)
		text("Waiting.",width*.75-textWidth("Waiting")/2,height*.4)
		if(millis()%2400>800)text("Waiting..",width*.75-textWidth("Waiting")/2,height*.4)
		if(millis()%2400>1600)text("Waiting...",width*.75-textWidth("Waiting")/2,height*.4)
		fill(0)
		rect(width/3,height*.7,width/3,height*.2)
		fill(200,0,0)
		text("Back",width*.5-textWidth("Back")/2,height*.845)
		fill(255,0,0)
		text("Back",width*.5-textWidth("Back")/2,height*.85)
	}
	if(opponent.id!=0){
		image(cursorIMG,opponent.x*width,opponent.y*height,14,20)
		textSize(width/40)
		if(mouseX>width*.1-width/20&&mouseX<width*.1+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)tooltip(desc[player.upgrades[0].type])
		if(mouseX>width*.275-width/20&&mouseX<width*.275+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)tooltip(desc[player.upgrades[1].type])
		if(mouseX>width*.45-width/20&&mouseX<width*.45+width/20&&mouseY>height*.85-width/20&&mouseY<height*.85+width/20)tooltip(desc[player.upgrades[2].type])
		background(0,anim[0])
		if(player==winner){
			anim[0]*=.9
			anim[0]+=width/100
			textSize(anim[0])
			fill(255,255,0)
			text("You Win!",width/2-textWidth("You Win!")/2,height/2)
		}else if(opponent==winner){
			anim[0]*=.9
			anim[0]+=width/100
			textSize(anim[0])
			fill(255,0,0)
			text("You Lose...",width/2-textWidth("You Lose...")/2,height/2)
		}
		
		if(anim[0]>width/10.01){
			socket.emit("quit",{game:game});
			screen=0
			anim[0]=0
		}
	}
}
function tooltip(x){
	fill(200)
	if(mouseX<width/2){
		rect(mouseX-10,mouseY-width/60,textWidth(x)+20,width/30)
		fill(0)
		text(x,mouseX,mouseY+width/120)
	}else{
		rect(mouseX-10,mouseY-width/60,-textWidth(x)-20,width/30)
		fill(0)
		text(x,mouseX-textWidth(x)-20,mouseY+width/120)
	}
}
function deckbuilder(){
	anim[0]*=.99
	anim[0]-=5
	background(200)
	
	var i=0;
	fill(0)
	rect(0,0,width*.1,height*.06)
	fill(200,0,0)
	text("Back",width*.05-textWidth("Back")/2,height*.05)
	fill(255,0,0)
	text("Back",width*.05-textWidth("Back")/2,height*.047)
	fill(100)
	rect(0,height*3/4,width,height/4)
	Object.keys(desc).forEach(function(key) {
		if(pages[anim[1]].cards.includes(key)){
			i++
			drawupgrade(width/30+i*width/10,(i%2)*width/7+width/6,key,"",0)
		}
	});
	var i=0;
	Object.keys(desc).forEach(function(key) {
		if(pages[anim[1]].cards.includes(key)){
			i++
			textSize(width/40)
			if(mouseX>width/30+i*width/10-width/20&&mouseX<width/30+i*width/10+width/20&&mouseY>(i%2)*width/7+width/6-width/20&&mouseY<(i%2)*width/7+width/6+width/20)tooltip(desc[key])
		}
	});
	for(var i=0;i<deck.length;i++){
		drawupgrade(width*i/10+width/10,height*15/16,deck[i],"",0)
	}
	fill(0)
	textSize(width/20)
	fill(155,0,0)
	text(pages[anim[1]].title,width/2-textWidth(pages[anim[1]].title)/2,width/20)
	fill(255,0,0)
	text(pages[anim[1]].title,width/2-textWidth(pages[anim[1]].title)/2-width/400,width/20-width/400)
	textSize(width*anim[0]/3000)
	fill(255,0,0,anim[0])
	
	text("Please add at least one card",width/2-textWidth("Please add at least one card")/2,height/2)
	textSize(width/40)
	
}
function windowResized(){
  resizeCanvas(windowWidth-10, windowHeight-20);
}
function draw(){
    if(!opponent)opponent={id:0};
	[title,deckbuilder,game][screen]()
}
